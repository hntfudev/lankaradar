"""
Create a folder inside backups named with timestamp
Copy results/*, pages/*, index.html, nGramLinks.html, Ngrams.txt, MasterBok.txt into the above folder

"""

import logging
import os
import datetime
import shutil, errno

logger = logging.getLogger(__name__)

def copyanything(src, dst):
    try:
        shutil.copytree(src, dst)
    except OSError as exc:
        if exc.errno == errno.ENOTDIR:
            shutil.copy(src, dst)
        else:
            logging.warning('Couldnt copy file ' + src+ ':error:' + str(exc.errno))


if __name__ == '__main__':
    logger.debug("backup started")

    if not os.path.exists('backups'):
        os.makedirs('backups')

    currentBackupDir = os.path.join('backups', datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S'))
    os.makedirs(currentBackupDir)

    currentResultsDir = os.path.abspath('results')
    destinationResultsDir = os.path.join(currentBackupDir, 'results')
    copyanything(currentResultsDir, destinationResultsDir)

    currentPagesDir = os.path.abspath('pages')
    destinationPagesDir = os.path.join(currentBackupDir, 'pages')
    copyanything(currentPagesDir, destinationPagesDir)

    shutil.rmtree(currentPagesDir)
    os.mkdir('pages')

    copyanything(os.path.abspath('index.html'), currentBackupDir)

    copyanything(os.path.abspath('MasterBook.txt'), currentBackupDir)

    copyanything(os.path.abspath('nGramLinks.xhtml'), currentBackupDir)

    copyanything(os.path.abspath('Ngrams.txt'), currentBackupDir)

