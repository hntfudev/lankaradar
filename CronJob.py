from subprocess import call

import os
import fnmatch
import time
import subprocess


if __name__ == '__main__':
    start_time = time.time()

    call(["python", 'Backup.py'])

    max_processes = 5
    processes = set()

    for file in os.listdir('scripts'):
        if fnmatch.fnmatch(file, 'scrape*.py'):
            processes.add(subprocess.Popen(["python", file], cwd=os.path.abspath('scripts')))
            if len(processes) >= max_processes:
                os.wait()
                processes.difference_update(
                    [p for p in processes if p.poll() is not None])

    # Check if all the child processes were closed
    for p in processes:
        if p.poll() is None:
            p.wait()

    call(["python", 'GenerateBook.py'])
    call(["python", 'ngrams.py', 'MasterBook.txt'])

    call(["python", 'GenerateNGramXML.py'])
    call(["python", 'SiteGenerator.py'])

    elapsed_time = time.time() - start_time
    print('Took {:.03f} seconds'.format(elapsed_time))
