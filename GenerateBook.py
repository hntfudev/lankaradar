import fnmatch
import os
import xml.etree.ElementTree as ET
import io
from sklearn.feature_extraction.text import CountVectorizer
from Utilis import tokenize
from Utilis import removeCommonWords
import nltk

output = ""

for file in os.listdir('./results'):
    if fnmatch.fnmatch(file, 'result_*.xhtml'):
        print file
        fullname = os.path.join('./results', file)
        tree = ET.parse(fullname)
        root = tree.getroot()
        for newsitem in root.findall('newsitem'):
            title = newsitem.find('heading')
            if(title is not None):
                #print(title.text)
                if(title.text is not None):
                    output = output + title.text

            content = newsitem.find('articleContent')
            if(content is not None):
                #print(content.text)
                if(content.text is not None):
                    output = output + content.text


output = tokenize(output)
output = removeCommonWords(output)



text_file = io.open("MasterBook.txt", 'wb')

text_file.write(output)
text_file.close()


