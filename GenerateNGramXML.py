# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division, print_function, unicode_literals


import fnmatch
import os
import xml.etree.ElementTree as ET
import io
import re
from Utilis import tokenize
import sys


from sumy.parsers.html import HtmlParser
from sumy.parsers.plaintext import PlaintextParser
from sumy.nlp.tokenizers import Tokenizer
#from sumy.summarizers.lsa import LsaSummarizer as Summarizer
from sumy.summarizers.lex_rank import LexRankSummarizer as Summarizer

from sumy.nlp.stemmers import Stemmer
from sumy.utils import get_stop_words

SENTENCES_COUNT = 3
LANGUAGE = "english"

reload(sys)
sys.setdefaultencoding("utf-8")

oRoot = ET.Element("html")

blacklist = ['had been', 'of a', 'your copyright', 'to be', 'as the', 'it is', 'in a', 'on the', 'will be']
newssources = []

def not_in_black_list(line):
    for badword in blacklist:
        if(badword in line):
            return False
    return True


for file in os.listdir('./results'):
    if fnmatch.fnmatch(file, 'result_*.xhtml'):
        print (file)
        realpath = os.path.join('./results', file)
        tree = ET.parse(realpath)
        root = tree.getroot()
        newssources.append(root)

with open('Ngrams.txt') as f:
    for line in f:
        if(line is not None):
            #print (line)
            if not_in_black_list(line):
                print(line)
                xmlNgram = ET.SubElement(oRoot, "ngram")
                #xmlNgram.text = line
                xmlLine = ET.SubElement(xmlNgram, "line")
                xmlLine.text = line
                xmlLinks = ET.SubElement(xmlNgram, "links")
                xmlAggContent = ET.SubElement(xmlNgram, "aggregateContent")
                xmlAggContent.text = ""
                for t in newssources:
                    print ('new newssource scanning')
                    for newsitem in t.findall('newsitem'):
                        bConsidered = False
                        title = newsitem.find('heading')
                        if (title is not None):
                            #print(title.text)
                            if (title.text is not None):
                                if(str(tokenize(line)) in str(tokenize(title.text))):
                                    xmlLink = ET.SubElement(xmlLinks, "link")
                                    xmlUrl = ET.SubElement(xmlLink, "url")
                                    xmlUrl.text = newsitem.find('link').text
                                    xmlTitle = ET.SubElement(xmlLink,"title")
                                    xmlTitle.text = newsitem.find('heading').text
                                    bConsidered = True

                        if(not bConsidered):
                            content = newsitem.find('articleContent')
                            if (content is not None):
                                # print(content.text)
                                if (content.text is not None):
                                    #print('line: ' + line)
                                    #print('context' + tokenize(content.text))
                                    if(str(tokenize(line)) in str(tokenize(content.text))):
                                        xmlAggContent.text = xmlAggContent.text + content.text
                                        xmlLink = ET.SubElement(xmlLinks, "link")
                                        xmlUrl = ET.SubElement(xmlLink, "url")
                                        xmlUrl.text = newsitem.find('link').text
                                        xmlTitle = ET.SubElement(xmlLink, "title")
                                        xmlTitle.text = newsitem.find('heading').text


                parser = PlaintextParser.from_string(xmlAggContent.text,Tokenizer(LANGUAGE))
                stemmer = Stemmer(LANGUAGE)

                summarizer = Summarizer(stemmer)
                summarizer.stop_words = get_stop_words(LANGUAGE)

                newString = ""
                for sentence in summarizer(parser.document, SENTENCES_COUNT):
                    newString = newString + str(sentence)

                xmlAggContent.text = newString




tree = ET.ElementTree(oRoot)

tree.write("nGramLinks.xhtml")

