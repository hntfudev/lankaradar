#!/usr/bin/env python

import os
from jinja2 import Environment, FileSystemLoader
import xml.etree.ElementTree as ET
import io
import re
import sys


PATH = os.path.dirname(os.path.abspath(__file__))
TEMPLATE_ENVIRONMENT = Environment(
    autoescape=False,
    loader=FileSystemLoader(os.path.join(PATH, 'templates')),
    trim_blocks=False)


def render_template(template_filename, context):
    return TEMPLATE_ENVIRONMENT.get_template(template_filename).render(context)


def create_index_html():

    tree = ET.parse('nGramLinks.xhtml')
    root = tree.getroot()

    urllist = []
    itemlist = []
    for ngram in root.findall('ngram'):
        urls = []
        list = []
        urlHeadingPair = []
        list.append(ngram.find('line').text)
        line = ngram.find('line').text
        #line.replace(' ', '-')
        line = re.sub("\s+", "-", line.strip())
        for links in ngram.findall('links'):
            if(links is not None):
                for link in links.findall('link'):
                    if(link is not None):
                        if(link.find('url') is not None and link.find('title') is not None):
                            urlHeadingPair = [link.find('url').text, link.find('title').text]
                            urls.append(urlHeadingPair)
        line = line + ".html"
        address = 'pages/' + line


        fname = address

        context = {
            'NGram':ngram.find('line').text,
            'urls': urls
        }
        #

        item = [ngram.find('line').text,urls, ngram.find('aggregateContent').text]
        itemlist.append(item)

        with open(fname, 'w') as f:
            html = render_template('page.html', context)
            f.write(html)

        list.append(address)
        urllist.append(list)

    fname = "index.html"

    context = {
        'urls': urllist,
        'itemlist':itemlist
    }
    #
    with open(fname, 'w') as f:
        html = render_template('index.html', context)
        f.write(html)

reload(sys)
sys.setdefaultencoding("utf-8")

def main():
    create_index_html()


########################################

if __name__ == "__main__":
    main()