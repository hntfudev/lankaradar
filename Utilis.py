
import re

def tokenize(string):
    """Convert string to lowercase and split into words (ignoring
    punctuation), returning list of words.
    """
    #print('input to tokenize')
    #print(string)
    list = re.findall(r'\w+', string.lower())
    #print('tokenized list')
    #print(list)
    out = ''
    for word in list:
        out = out + str(word) + ' '
    #print('printing tokenized:')
    #print out

    return out

def removeCommonWords(out):
    out = out.replace(' as the', ' ')
    out = out.replace(' to be', ' ')
    out = out.replace(' to a', ' ')
    out = out.replace(' had to', ' ')
    out = out.replace('september', ' ')
    out = out.replace('2017', ' ')
    out = out.replace('sri lanka', ' ')
    out = out.replace(' the ', ' ')
    out = out.replace(' in ', ' ')
    out = out.replace(' of ', ' ')
    out = out.replace(' to ', ' ')
    out = out.replace(' as a ', ' ')
    out = out.replace(' is a ', ' ')
    out = out.replace(' for a ', ' ')
    out = out.replace(' it ', ' ')
    out = out.replace(' he ', ' ')
    out = out.replace(' have ', ' ')
    out = out.replace(' she ', ' ')
    out = out.replace(' today ', ' ')
    out = out.replace(' stated that ', ' ')
    out = out.replace(' reported that ', ' ')
    out = out.replace(' pointed out ', ' ')
    out = out.replace(' on ', ' ')
    out = out.replace(' at ', ' ')
    out = out.replace(' with ', ' ')
    out = out.replace(' more ', ' ')
    out = out.replace(' a ', ' ')
    out = out.replace(' and ', ' ')
    out = out.replace(' not ', ' ')
    out = out.replace(' did ', ' ')
    out = out.replace(' had ', ' ')
    out = out.replace(' which ', ' ')
    out = out.replace(' would ', ' ')
    out = out.replace(' are ', ' ')
    out = out.replace(' an ', ' ')
    out = out.replace(' should ', ' ')
    out = out.replace(' stated ', ' ')
    out = out.replace(' said ', ' ')
    out = out.replace(' that ', ' ')
    out = out.replace(' there ', ' ')
    out = out.replace(' was ', ' ')
    out = out.replace('the ', ' ')
    out = out.replace(' more ', ' ')
    out = out.replace(' s ', ' ')
    out = out.replace(' were ', ' ')
    out = out.replace(' rights ', ' ')
    out = out.replace(' copyright ', ' ')
    out = out.replace('copyright ', ' ')
    out = out.replace(' ada ', ' ')
    out = out.replace(' derana ', ' ')
    out = out.replace(' all reserved ', ' ')
    out = out.replace('derana', ' ')
    out = out.replace('all reserved', ' ')
    out = out.replace('more a', ' ')
    out = out.replace('support iframes', ' ')
    out = out.replace('browser does', ' ')
    out = out.replace('your browser', ' ')
    out = out.replace('will be', ' ')
    out = out.replace('has been', ' ')
    out = out.replace('such as', ' ')
    out = out.replace('could be', ' ')
    out = out.replace('this is', ' ')
    out = out.replace('last updated', ' ')
    out = out.replace('updated sep', ' ')
    out = out.replace('your your', ' ')
    out = out.replace('those who', ' ')












    return out