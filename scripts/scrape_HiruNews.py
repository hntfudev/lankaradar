from bs4 import BeautifulSoup
import requests
import xml.etree.ElementTree as ET
from Utilis import removeCommonWords

root = ET.Element("html")


r = requests.get("http://www.hirunews.lk/local-news.php")

data = r.text

soup = BeautifulSoup(data)

blacklist = ['hirufm', 'hirutv', 'sunfm', 'youtube', 'goldfm', 'sooriyan', 'shaafm', 'lk/balaya', 'lk/salakuna', 'twitter', 'picture-story','entertainment',
             'hotvideo', 'Logo', 'Home', '#', 'all-hard-talk', 'janamathaya', 'hot-video', 'all-awards', 'webupload', 'rss', 'hiruads']

def check_in_blacklist(string):
    for item in blacklist:
        if(item in string):
            return True
    return False

if __name__ == '__main__':
    list = []
    for link in soup.find_all('a', href=True):
        if(not check_in_blacklist(link.prettify())):
            xmlnewsitem = ET.SubElement(root, "newsitem")
            print(link['href'])

            if (link['href'] not in list): #duplicate check
                list.append(link['href'])
                xmlLink = ET.SubElement(xmlnewsitem, "link")
                xmlLink.text = link['href']
                artk = requests.get(link['href'])
                artkData = artk.text
                soupk = BeautifulSoup(artkData)
                heading = soupk.find("div", { "class" : "lts-cntp2" })
                if(heading):
                    print(heading.text)
                    xmlheading = ET.SubElement(xmlnewsitem, "heading")
                    xmlheading.text = heading.text

                    articleContent = ""
                    for para in soupk.find_all('p'):
                        print(para.string)
                        if (para.string):
                            articleContent = articleContent + " " + removeCommonWords(para.string.lower())
                    xmlArticlContent = ET.SubElement(xmlnewsitem, "articleContent")
                    xmlArticlContent.text = articleContent




    tree = ET.ElementTree(root)

    tree.write("../results/result_HiruNews.xhtml")



