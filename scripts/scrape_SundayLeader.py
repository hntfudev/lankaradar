from bs4 import BeautifulSoup
import requests
import xml.etree.ElementTree as ET
from Utilis import removeCommonWords

root = ET.Element("html")


r = requests.get("http://www.thesundayleader.lk/category/news/")

data = r.text

soup = BeautifulSoup(data)


if __name__ == '__main__':
    list = []
    for link in soup.find_all('a', href=True):
        if("2017" in link.prettify() and "uploads" not in link.prettify()):
            xmlnewsitem = ET.SubElement(root, "newsitem")
            print(link['href'])

            if (link['href'] not in list): #duplicate check
                list.append(link['href'])
                xmlLink = ET.SubElement(xmlnewsitem, "link")
                xmlLink.text = link['href']
                artk = requests.get(link['href'])
                artkData = artk.text
                soupk = BeautifulSoup(artkData)
                heading = soupk.find('h1')
                print(heading.string)
                xmlheading = ET.SubElement(xmlnewsitem, "heading")
                xmlheading.text = heading.string

                articleContent = ""
                for para in soupk.find_all('p'):
                    print(para.string)
                    if (para.string):
                        articleContent = articleContent + " " + removeCommonWords(para.string.lower())
                xmlArticlContent = ET.SubElement(xmlnewsitem, "articleContent")
                xmlArticlContent.text = articleContent




    tree = ET.ElementTree(root)

    tree.write("../results/result_SundayLeader.xhtml")



