from bs4 import BeautifulSoup
import requests
import xml.etree.ElementTree as ET
from Utilis import removeCommonWords
root = ET.Element("html")


r = requests.get("http://www.island.lk/index.php?page_cat=news-section&page=news-section&code_title=58")

data = r.text

soup = BeautifulSoup(data)


if __name__ == '__main__':
    for link in soup.find_all('a', href=True):
        if("article-details" in link.prettify()):
            xmlnewsitem = ET.SubElement(root, "newsitem")
            print(link['href'])
            xmlLink = ET.SubElement(xmlnewsitem, "link")
            xmlLink.text = link['href']
            artk = requests.get(link['href'])
            artkData = artk.text
            soupk = BeautifulSoup(artkData)
            heading = soupk.find('h1')
            print(heading.string)
            xmlheading = ET.SubElement(xmlnewsitem, "heading")
            xmlheading.text = heading.string

            articleContent = ""
            for para in soupk.find_all('p'):
                #print(para.string)
                if(para.string):
                    articleContent = articleContent + " " + removeCommonWords(para.string.lower())

            xmlArticlContent = ET.SubElement(xmlnewsitem, "articleContent")
            xmlArticlContent.text = articleContent

    tree = ET.ElementTree(root)
    tree.write("../results/result_TheIsland.xhtml")




